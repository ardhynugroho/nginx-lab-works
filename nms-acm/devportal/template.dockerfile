FROM ubuntu:focal

RUN --mount=type=secret,id=nginx-crt,dst=/etc/ssl/nginx/nginx-repo.crt,mode=0644 \
    --mount=type=secret,id=nginx-key,dst=/etc/ssl/nginx/nginx-repo.key,mode=0644 \

# Install prerequisite packages:
    apt-get update && apt-get install -y apt-transport-https lsb-release ca-certificates wget gnupg2 \

# Download and add the NGINX signing keys:
    && wget https://cs.nginx.com/static/keys/nginx_signing.key \
    && apt-key add nginx_signing.key \

# Add NGINX Plus & NMS repository:
    && printf "deb https://pkgs.nginx.com/plus/ubuntu `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/nginx-plus.list \
    && printf "deb https://pkgs.nginx.com/nms/ubuntu `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/nms.list \

# Download the apt configuration to `/etc/apt/apt.conf.d`:
    && wget -P /etc/apt/apt.conf.d https://cs.nginx.com/static/files/90pkgs-nginx \

# Update the repository and install the most recent version of the NGINX App Protect WAF package (which includes NGINX Plus):
    && apt-get update && DEBIAN_FRONTEND="noninteractive" TZ=Asia/Jakarta apt-get install -y nginx-devportal nginx-devportal-ui \
    && wget --no-check-certificate https://__NMSADDR__/install/nginx-agent -O - | sh \

# setup database backend, using SQLITE
    && echo 'DB_TYPE="sqlite"' | tee -a /etc/nginx-devportal/devportal.conf \
    && echo 'DB_PATH="/var/lib/nginx-devportal"' | tee -a /etc/nginx-devportal/devportal.conf \

# Forward request logs to Docker log collector:
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# Copy configuration files:
COPY entrypoint.sh /root/

CMD ["sh", "/root/entrypoint.sh"]
