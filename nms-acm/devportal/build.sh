#!/bin/bash
set -x
if [ $# -eq 1 ]; then
    if [[ -f $(pwd)/nginx-repo.crt && -f $(pwd)/nginx-repo.crt ]]; then
        sed "s/__NMSADDR__/$1/g" template.dockerfile > dockerfile
        DOCKER_BUILDKIT=1 docker build  --no-cache --secret id=nginx-key,src=nginx-repo.key --secret id=nginx-crt,src=nginx-repo.crt -t devportal .
    else
        echo "nginx-repo.crt and/or nginx-repo.key does not exists in $(pwd)"
        exit 1
    fi
else
    echo "usage: $0 NMS_IP_ADDRESS"
fi