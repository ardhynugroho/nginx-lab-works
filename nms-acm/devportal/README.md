Build

```
DOCKER_BUILDKIT=1 docker build --no-cache --secret id=nginx-key,src=nginx-repo.key --secret id=nginx-crt,src=nginx-repo.crt -t devportal .
```

Publish

```
docker login
docker image tag devportal and0cker/belajar:devportal
docker push and0cker/belajar:devportal
```