# NGINX API Connectivity Manager Lab Guide

In this guide:

[TOC]

Pre-requisite: NGINX Instance Manager install. See https://docs.nginx.com/nginx-management-suite/admin-guides/installation/install-guide/ for more details

# Install API Connectivity Manager Module

@ DC3: NMS
```
sudo apt-get update
sudo apt-get install -y nms-api-connectivity-manager
```

Edit `/lib/systemd/system/nms-acm.service` file, then remove `clickhouse-server.service` entry in `Requires=` value

Before edit:
```
Requires=network-online.target clickhouse-server.service nginx.service nms-core.service nms-dpm.service nms-ingestion.service
```

After edit:
```
Requires=network-online.target nginx.service nms-core.service nms-dpm.service nms-ingestion.service
```

Reload `systemd` to relect updated `/lib/systemd/system/nms-acm.service` file

@ DC3: NMS
```
sudo systemctl daemon-reload
```

Please follow the next steps to Start the software:

Start NGINX web server

@ DC3: NMS
```
sudo systemctl start nginx
```

If NGINX is already running, reload it

@ DC3: NMS
```
sudo systemctl restart nginx
```

to ensure the service starts whenever the system boots

@ DC3: NMS
```
sudo systemctl enable nms-acm
```

Start NGINX Management Suite ACM service

@ DC3: NMS
```
sudo systemctl start nms-acm
```

Navigate to *Launchpad* then check the new installed NMS module is available in the GUI

![](img/ss-acm.png)

# Deploy The Data Plane (APIGW)

Run a NGINX Plus with NAP docker container and expose port 7000

*Note: The `dockerfile` to build the `apigw` image is available under apigw folder*

@ DC3: NGINX+
```
docker run --rm --hostname apigw --name apigw -p 7000:80 -d apigw
```

# Deploy Devportal

Devportal install in dedicated machine is recommended. As it require port 80 & 81.
In this guide, the Devportal will be installed as docker container in NMS machine, because NMS uses port 443 and leave port 80 and 81 free.

*Note: The `dockerfile` to build the `devportal` image is available under devportal folder*

@ DC3: NMS

```
docker run --rm -h devportal --name devportal -p 80:80 -p 81:81 -d devportal
```

# Connect to ACM and create the infrastructure environment

1. Create a Workspace and name it `team-sentence`

    ![](img/ss-infra-ws.png)

1. Create an environment
    - Name : sentence-env
    - Type : Non-Prod

1. Add an API Gateway cluster
    - Name : api-cluster
    - Hostname : api.sentence.com

1. Add a Developer portal cluster
    - Name : devportal-cluster
    - Hostname : dev.sentence.com

    ![](img/ss-add-env.png)
    
1. The environment is now created and you can see the commands to execute to link NGINX instances to your NMS.

# Link the API GW and Dev Portal

1. Enter API GW instance

    ```
    docker exec -it apigw bash
    ```

1. Link it to the ACM

    ```
    nginx-agent --instance-group api-cluster &
    ```

1. Enter `devportal` instance

    ```
    docker exec -it devportal bash
    ```

1. Link it to the ACM

    ```
    nginx-agent --instance-group devportal-cluster &
    ```

1. Check the status in ACM GUI

    1. In API Gateway Clusters section, click on name `api-cluster`
    1. Scroll down and check your API-GW instance is linked and green

        ![](img/ss-apigw-status.png)
    
    1. Switch to the Dev-Portal by clicking on the cluster top menu
    1. You can see your Nginx DevPortal instance GREEN, but also a way to customize the DevPortal

1. Switch back to your Infrastructure Environment screen. And check the Job Status

# Create API service v1.0

Download OpenAPI spec file (OASv3) via SwaggerHub https://app.swaggerhub.com/apis/F5EMEASSA/API-Sentence-2022/v1

![](img/ss-dl-swagger.png)
    
Select `Service` in the *API Connectivity Manager* menu in the left and 

1. Create service workspace named `sentence-app`
1. Publish a API service
    - Backend Service:
        - Name: sentence-svc
        - Service Target Hostname (or IP): 10.1.1.7
        - Service Target Transport Protocol: HTTP
        - Service Target Port: 7000
    - API Proxy
        - Name: sentence-api
        - Select API spec
        - Gateway Proxy Hostname: api.sentence.com
    - Developer Portal
        - Tick _Also publish API to developer portal_
        - Portal Proxy Hostname: dev.sentence.com

![](img/ss-publish-api.png)

You may need to add following entry in `/etc/hosts` file in ACM node.

@ DC3: NMS
```
10.1.1.5 acm.dev.sentence.com
```

# Test Application

## Deploy

@ DC3: NGINX+ 
```
git clone https://gitlab.com/ardhynugroho/nginx-lab-works.git

cd nginx-lab-works/nms-acm/sentence-app

sh sentence-app.sh
```

## Test direct API access

@ DC3: NMS 
```
curl http://10.1.1.10:9001/health
curl http://10.1.1.10:9001/api/sentence
curl http://10.1.1.10:9001/api/sentence/adjectives
curl http://10.1.1.10:9001/api/sentence/animals
curl http://10.1.1.10:9001/api/sentence/colors
curl http://10.1.1.10:9001/api/sentence/locations

curl http://10.1.1.10:9003/adjectives
curl http://10.1.1.10:9004/animals
curl http://10.1.1.10:9002/colors
curl http://10.1.1.10:9005/locations
```

## Test Sentence API v1 access from ACM

**<!> Make sure `api.sentence.com` resolved as 10.1.1.10**

Required request information

```mermaid
graph TD
  A[client] --> |/v1/api/adjectives| B[APIGW]
  B --> |/api/sentence/adjectives| C[Backend APP]
```

Change ingress configuration to strip the base path

![](img/ss-strip-base-path.png)

Then change `Context Root` parameter in API Proxy Backends to `/api/sentence`

![](img/ss-change-context-root.png)

Let's do the test over ACM

```
curl api.sentence.com:7000/v1/api/adjectives
curl api.sentence.com:7000/v1/api/animals
curl api.sentence.com:7000/v1/api/locations
```

# Update new API spec v2

Download API YAML Unresolved from
https://app.swaggerhub.com/apis/F5EMEASSA/API-Sentence-2022/v2

1. Upload the new API to API Docs

    ![](img/ss-publish-new-api.png)

1. Change API spec in API proxy configuration

    ![](img/ss-change-api-spec.png)

Check the developer portal

![](img/ss-devportal-api-v2.png)

Check new API endpoint

```
curl api.sentence.com:7000/v2/api/colors
```