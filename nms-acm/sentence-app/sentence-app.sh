#!/bin/sh

docker run --rm -d -p 9001:8080 --network sentence -h generator --name generator -v $(pwd)/generator.py:/generator.py matt262810/sentence-generator:v1.0

docker run --rm -d -p 9002:8080 --network sentence -h colors --name colors matt262810/sentence-colors:v1.0

docker run --rm -d -p 9003:8080 --network sentence -h adjectives --name adjectives matt262810/sentence-adjectives:v1.0

docker run --rm -d -p 9004:8080 --network sentence -h animals --name animals matt262810/sentence-animals:v1.0

docker run --rm -d -p 9005:8080 --network sentence -h locations --name locations matt262810/sentence-locations:v1.0

docker run --rm -d -p 9006:8080 --network sentence -h backgrounds --name backgrounds matt262810/sentence-backgrounds:v1.0
