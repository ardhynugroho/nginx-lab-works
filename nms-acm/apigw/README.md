Build

```
DOCKER_BUILDKIT=1 docker build  --no-cache --secret id=nginx-key,src=nginx-repo.key --secret id=nginx-crt,src=nginx-repo.crt -t apigw .
```

Publish

```
docker login
docker image tag apigw and0cker/belajar:apigw
docker push and0cker/belajar:apigw
```