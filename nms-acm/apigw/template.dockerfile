FROM ubuntu:focal

RUN --mount=type=secret,id=nginx-crt,dst=/etc/ssl/nginx/nginx-repo.crt,mode=0644 \
    --mount=type=secret,id=nginx-key,dst=/etc/ssl/nginx/nginx-repo.key,mode=0644 \

# Install prerequisite packages:
    apt-get update && apt-get install -y apt-transport-https lsb-release ca-certificates wget gnupg2 \

# Download and add the NGINX signing keys:
    && wget https://cs.nginx.com/static/keys/nginx_signing.key \
    && apt-key add nginx_signing.key \
    && wget https://cs.nginx.com/static/keys/app-protect-security-updates.key \
    && apt-key add app-protect-security-updates.key \

# Add NGINX Plus repository:
    && printf "deb https://pkgs.nginx.com/plus/ubuntu `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/nginx-plus.list \

# Add NGINX App Protect WAF repositories:
    && printf "deb https://pkgs.nginx.com/app-protect/ubuntu `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/nginx-app-protect.list \
    && printf "deb https://pkgs.nginx.com/app-protect-security-updates/ubuntu `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/app-protect-security-updates.list \

# Download the apt configuration to `/etc/apt/apt.conf.d`:
    && wget -P /etc/apt/apt.conf.d https://cs.nginx.com/static/files/90pkgs-nginx \

# Update the repository and install the most recent version of the NGINX App Protect WAF package (which includes NGINX Plus):
    && apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y app-protect nginx-plus-module-njs \

# --------->>>>>> Install nginx-agent from NMS. Update the NMS IP address
    && wget --no-check-certificate https://__NMSADDR__/install/nginx-agent -O - | sh \

# ini harus di enable, supaya muncul di NMS
    && echo "nginx_app_protect:" >> /etc/nginx-agent/nginx-agent.conf \ 
    && echo "  report_interval: 15s" >> /etc/nginx-agent/nginx-agent.conf \

# Forward request logs to Docker log collector:
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# Copy configuration files:
COPY nginx.conf custom_log_format.json /etc/nginx/
COPY default.conf /etc/nginx/conf.d
COPY entrypoint.sh /root/

CMD ["sh", "/root/entrypoint.sh"]
