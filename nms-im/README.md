# NGINX Instance Manager Lab Guide

In this guide:

[TOC]

This lab guide to install NGINX Instance Manager

See https://docs.nginx.com/nginx-management-suite/admin-guides/installation/install-guide/ for more details

**Note:** The _@ vm_location_ denotes the location of command execution.

## Install ClickHouse Server

@ DC3: NMS
```
docker run -d -p 8123:8123 -p 9000:9000 --name clickhouse-server --ulimit nofile=262144:262144 -v /var/lib/clickhouse:/var/lib/clickhouse/ -v /var/lib/clickhouse-server:/var/log/clickhouse-server/ clickhouse/clickhouse-server
```

Test the connection to clickhouse server

@ DC3: NMS
```
docker exec -it clickhouse-server clickhouse-client
```

## NMS Repository Setup

To access the NGINX Management Suite repo, you’ll need to add appropriate cert and key files to the etc/ssl/nginx folder.

@ DC3: NMS
```
sudo mkdir -p /etc/ssl/nginx
sudo cp nginx-repo.crt /etc/ssl/nginx
sudo cp nginx-repo.key /etc/ssl/nginx

printf "deb https://pkgs.nginx.com/nms/ubuntu `lsb_release -cs` nginx-plus\n" | sudo tee /etc/apt/sources.list.d/nms.list
sudo wget -q -O /etc/apt/apt.conf.d/90pkgs-nginx https://cs.nginx.com/static/files/90pkgs-nginx

wget -O /tmp/nginx_signing.key https://cs.nginx.com/static/keys/nginx_signing.key
sudo apt-key add /tmp/nginx_signing.key
```

## Install NMS

@ DC3: NMS
```
sudo apt-get update
sudo apt-get install -y nms-instance-manager

sudo sh -c 'echo "admin:$(echo "default" | openssl passwd -6 -noverify -stdin)" > /etc/nms/nginx/.htpasswd'

sudo systemctl enable nms
sudo systemctl enable nms-core
sudo systemctl enable nms-dpm
sudo systemctl enable nms-ingestion
sudo systemctl enable nms-integrations

sudo systemctl start nms
sudo systemctl start nms-core
sudo systemctl start nms-dpm
sudo systemctl start nms-ingestion
sudo systemctl start nms-integrations
```

Reload nginx

@ DC3: NMS
```
sudo systemctl reload nginx
```

## Login

Open the NMS Web UI, navigate to *ACCESS -> NMS GUI*

![](img/ss-akses-nms-gui.png)

Now you can login with `admin/default` password

![NMS launchpad](img/ss-launchpad.png)

## Upload NMS License

From the launchpad, click *Settings* menu pad then click <kbd>Upload License</kbd>. 

Browse the downloaded lincense file (`.lic` extension) then click <kbd>Upload</kbd>

## Add an NGINX instance to Instance Manager

Run a NGINX docker image

@ data-plane
```
docker run --rm --hostname test --name test -p 8000:80 -d nginx
```

Enter the container

@ data-plane
```
docker exec -it test bash
```

Install nginx agent. In this lab NMS server address is 10.1.1.5

@ data-plane
```
apt update
apt install -y gnupg2
curl -k https://10.1.1.5/install/nginx-agent | sh
```

Run the nginx agent in foreground to show the logging

@ data-plane
```
nginx-agent
```

Navigate to module *Instance Manager* then select *Instances*. Check created & linked NGINX OSS instance named *test*.

## Configure instance from Instance Manager

1. Navigate to *Instances*
1. Click on *test* instance name
1. Click *Edit Config* at upper right menu

    ![](img/ss-config-edit.png)

1. Select the *default.conf* file from file browser
1. Add following config inside `server` context

    ```
    location /test {
        return 200 "Mantap jiwa!";
    }
    ```
1. Click *Publish*

Test the new config

@ DC3: NMS
```
curl http://10.1.1.6:8000/test
```

## Explore The NMS GUI

Instance Scan

![](img/ss-instance-scan-result.png)

Managed instances

![](img/ss-instances.png)

Instance group

![](img/ss-instance-group.png)

Instance detail

![](img/ss-instance-detail.png)

Instance metrics

![](img/ss-instance-metrics-summary.png)

![](img/ss-instance-metrics.png)