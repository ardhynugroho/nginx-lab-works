#!/bin/sh

install() {
    # cek repo crt & key
    if [[ -f "$(pwd)/nginx-repo.crt" && -f "$(pwd)/nginx-repo.key" ]]; then
        # run clickhouse-server
        sudo docker run --rm -d -p 8123:8123 -p 9000:9000 --name clickhouse-server --ulimit nofile=262144:262144 -v /var/lib/clickhouse:/var/lib/clickhouse/ -v /var/lib/clickhouse-server:/var/log/clickhouse-server/ clickhouse/clickhouse-server

        # repo setup
        sudo mkdir -p /etc/ssl/nginx
        sudo cp nginx-repo.crt /etc/ssl/nginx
        sudo cp nginx-repo.key /etc/ssl/nginx
        printf "deb https://pkgs.nginx.com/nms/ubuntu `lsb_release -cs` nginx-plus\n" | sudo tee /etc/apt/sources.list.d/nms.list
        sudo wget -q -O /etc/apt/apt.conf.d/90pkgs-nginx https://cs.nginx.com/static/files/90pkgs-nginx
        wget -O /tmp/nginx_signing.key https://cs.nginx.com/static/keys/nginx_signing.key
        sudo apt-key add /tmp/nginx_signing.key

        # install
        sudo apt-get update
        sudo apt-get install -y nms-instance-manager nms-api-connectivity-manager

        # set default credential
        sudo sh -c 'echo "admin:$(echo "default" | openssl passwd -6 -noverify -stdin)" > /etc/nms/nginx/.htpasswd'

        # Remove clickhouse-server dependency since it is running as docker container
        sudo sed -i 's/clickhouse-server.service//g' /lib/systemd/system/nms-acm.service

        sudo systemctl daemon-reload

        sudo systemctl enable nms
        sudo systemctl enable nms-acm
        sudo systemctl enable nms-core
        sudo systemctl enable nms-dpm
        sudo systemctl enable nms-ingestion
        sudo systemctl enable nms-integrations

        sudo systemctl start nms
        sudo systemctl start nms-acm
        sudo systemctl start nms-core
        sudo systemctl start nms-dpm
        sudo systemctl start nms-ingestion
        sudo systemctl start nms-integrations

        # reload nginx
        sudo systemctl reload nginx
    else
        echo "cannot find file $(pwd)/nginx-repo.crt and/or $(pwd)/nginx-repo.key"
    fi

}

remove() {
        sudo docker stop clickhouse-server
        sudo rm -rf /etc/ssl/nginx
        sudo apt remove -y nms-instance-manager nms-api-connectivity-manager
}

case "$1" in
    install) install ;;
    remove) remove ;;
    *) echo "usage $0 install|remove" >&2
       exit 1
       ;;
esac