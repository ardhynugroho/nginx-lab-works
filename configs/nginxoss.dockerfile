FROM nginx
RUN apt-get update; apt-get upgrade -y; apt-get install -y gnupg2; curl -k https://10.1.1.11/install/nginx-agent | sh
COPY entrypoint.sh /root/
CMD ["sh", "/root/entrypoint.sh"]
