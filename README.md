# ID BOOTCAMP JUMPSTART LAB GUIDES

1. NGINX Management Suite Instance Manager
1. NGINX Management Suite API Connectivity Manager

# Lab Topology

```mermaid
graph TD
internet --- net1((subnet<br>10.1.1.0/24))
net1 --- |10.1.1.8| client
net1 --- |10.1.1.7| ctl[control]
net1 --- |10.1.1.6| wk1[worker-1]
net1 --- |10.1.1.5| wk2[worker-2]
subgraph control-plane
ctl --- |:443| nms
ctl --- docker.ctl[docker]
docker.ctl --- |:80 :81| devportal([devportal])
docker.ctl --- |:9000| clickhouse([clickhouse])
end
subgraph worker-1
wk1 --- docker.wk1[docker]
docker.wk1 --- |:7000| apigw([apigw])
end
subgraph worker-2
wk2 --- docker.wk2[docker]
end
```

# Notes

## Firefox Browser

1. SSH to DC3: NMS deployment
1. Deploy firefox docker image

```
docker run -d \
    --name=firefox \
    -p 5800:5800 \
    -v /docker/appdata/firefox:/config:rw \
    --shm-size 2g \
    jlesage/firefox
```
![](img/ss-ff.png)

## GNU Screen

It is recommended to exec `screen` command after first remote login via SSH. This is very useful to get your session back after lost SSH connection.

To retrieve your screen after lost SSH connection, re-login over SSH and 

```
screen -r
```

Here some shortcut to navigate over the screens:

1. New screen &rarr; C-a C-c
1. Scroll over screen &rarr; C-a C-a
1. Jump to screen number &rarr; C-a [0-9]
1. Screen selection menu &rarr; C-a "
1. Set screen tittle to be shown in the selection menu &rarr; C-a A

:)